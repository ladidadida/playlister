#!/usr/bin/env bash


if [ -z "$NAUTILUS_SCRIPT_CURRENT_URI" ]; then
  echo "Has to be called from Nautilus Scripts with EnvVars set correctly."
  exit 0
fi

SCRIPT_DIR=$(cd $(dirname "$(realpath "${BASH_SOURCE[0]}")") && pwd)

if type "playlister" > /dev/null; then
  PLAYLISTER_EXEC="playlister"
elif type "playlister.py" > /dev/null; then
  PLAYLISTER_EXEC="playlister.py"
elif [ -f "$SCRIPT_DIR/../playlister.py" ]; then
  PLAYLISTER_EXEC="$SCRIPT_DIR/../playlister.py"
fi

if [ -z "$PLAYLISTER_EXEC" ]; then
  echo "playlister not found on system. Add to Path or specify 'PLAYLISTER_EXEC'"
  exit 1
fi

PLAYLISTER_PATHS_LIST=
while read PATH
do
  if [ -n "$PATH" ]; then
    if [ "${PATH:0:7}" == "file://" ]; then
      PATH=${PATH:7}
    fi
    PLAYLISTER_PATHS_LIST="$PLAYLISTER_PATHS_LIST -p $PATH"
  fi
done <<< $NAUTILUS_SCRIPT_SELECTED_URIS

if [ "${NAUTILUS_SCRIPT_CURRENT_URI:0:7}" == "file://" ]; then
  PLAYLISTER_OUTFILE="${NAUTILUS_SCRIPT_CURRENT_URI:7}/Playlist.xspf"
else
  PLAYLISTER_OUTFILE="$NAUTILUS_SCRIPT_CURRENT_URI/Playlist.xspf"
fi

echo $PLAYLISTER_EXEC $PLAYLISTER_PATHS_LIST -f XSPF "$PLAYLISTER_OUTFILE"
$PLAYLISTER_EXEC $PLAYLISTER_PATHS_LIST -f XSPF "$PLAYLISTER_OUTFILE"
