#!/usr/bin/python3
import click
from pathlib import Path
import mutagen

from urllib.parse import unquote, urlparse, quote

from typing import List


DEFAULT_SUPPORTED_EXTENSIONS = [".mp3", ".mp4", ".m4a", ".flac", ".ogg", ".avi", ".flv"]

def clean_path(uri_or_path):
    path = unquote(uri_or_path)
    if "file://" in path:
        path = urlparse(path).path
    return path


class Playlister():
    def __init__(self, paths: List[Path], allowed_extensions: List[str]=None, sorting: str=None, title_type: str=None, base_path: Path=None):
        self.media_files = []
        self.allowed_extensions = allowed_extensions
        for path in (Path(clean_path(string)) for string in paths):
            if path.is_dir():
                self.media_files.extend((Playlister.get_media_file_info(file) for file in Playlister.deep_scan_dir(path, allowed_extensions)))
            elif path.is_file():
                if path.suffix in allowed_extensions:
                    self.media_files.append(Playlister.get_media_file_info(path))

        if sorting:
            self.sort_media(sorting)

        if title_type:
            self.update_title_type(title_type, base_path=base_path)

    @staticmethod
    def get_media_file_info(media_file):
        info_dict={
            "path": media_file,
            "title": f"{media_file.stem}",
            "length": ""
        }

        try:
            media_file_tags=mutagen.File(media_file)
        except:
            #print(f"Error: Unable to get TAG from path: {media_file}")
            media_file_tags = None

        if media_file_tags:
            info_dict["length"]=round(media_file_tags.info.length)
        
        return info_dict

    @staticmethod
    def deep_scan_dir(directory, allowed_extensions=None):
        """
        Generator function that yields file paths of a given 'directory'

        Parameter:
          - directory: directory that shall be read
          - allowed_extensions[optional]: list of file extensions - files that do not match are filtered out
        """
        for path in Path(directory).rglob("*"):
            if allowed_extensions and path.suffix not in allowed_extensions:
                continue
            yield path

    def update_title_type(self, type, **kwargs):
        for media_file in self.media_files:
            if type == "simple":
                media_file["title"] = media_file["path"].stem
            if type == "simple_with_rel_path":
                if not "base_path" in kwargs or not kwargs["base_path"]:
                    return
                relative_path_parts = media_file["path"].parent.relative_to(Path(kwargs["base_path"])).parts
                if len(relative_path_parts) > 0:
                    title = "[ " + " / ".join(relative_path_parts) + " ] " + media_file["path"].stem
                else:
                    title = media_file["path"].stem
                print(title)
                media_file["title"] = title
            if type == "complex":
                if not "format" in kwargs:
                    return
                pass
            if type == "fromfunction":
                if not "func" in kwargs:
                    return
                pass

    def sort_media(self, method):
        if method == "ascending":
            self.media_files.sort(key=lambda item: item.get("path"))
        if method == "descending":
            self.media_files.sort(key=lambda item: item.get("path"), reverse=True)
        if method == "random":
            import random
            random.shuffle(self.media_files)

    def write_media_info_to_m3u_file(self, m3u_file, relative_paths=False, encoding=None):
        with open(m3u_file, "w", encoding=encoding) as out_stream:
            out_stream.write("#EXTM3U \n\n")

            for media_info in self.media_files:
                out_stream.write(f"#EXTINF:{media_info['length']}, {media_info['title']}\n")
                if relative_paths:
                    media_path = Path(media_info["path"]).resolve().relative_to(Path(m3u_file).resolve().parent)
                else:
                    media_path = media_info["path"]
                out_stream.write(f"{media_path}\n\n")

    def write_media_info_to_pls_file(self, pls_file, relative_paths=False, encoding=None):
        with open(pls_file, "w", encoding=encoding) as out_stream:
            out_stream.write("[playlist]\n\n")

            for num, media_info in enumerate(self.media_files, start=1):
                if relative_paths:
                    media_path = Path(media_info["path"]).relative_to(Path(pls_file).resolve().parent)
                else:
                    media_path = media_info["path"]
                out_stream.write(f"File{num}={media_path}\n")
                out_stream.write(f"Title{num}={media_info['title']}\n\n")

            out_stream.write(f"NumberOfEntries={num}")

    def write_media_info_to_xspf_file(self, xspf_file, relative_paths=False):
        with open(xspf_file, "w") as out_stream:
            out_stream.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n")
            out_stream.write("<playlist version=\"1\" xmlns=\"http://xspf.org/ns/0/\">\n")
            out_stream.write("  <trackList>\n")

            for num, media_info in enumerate(self.media_files, start=1):
                if relative_paths:
                    media_path = Path(media_info["path"]).relative_to(Path(xspf_file).resolve().parent)
                else:
                    media_path = media_info["path"]
                out_stream.write("    <track>\n")
                out_stream.write(f"      <title>{media_info['title']}</title>\n")
                out_stream.write(f"      <location>{media_path}</location>\n")
                out_stream.write("    </track>\n")

            out_stream.write("  </trackList>\n")
            out_stream.write("</playlist>\n")



@click.command()
@click.option("-p", "--path", "paths", multiple=True, type=click.Path(resolve_path=True, exists=True))
@click.option("-e", "--extension", "extensions", multiple=True, type=str, default=DEFAULT_SUPPORTED_EXTENSIONS)
@click.option("--ascending-sorting", "sorting", flag_value="ascending")
@click.option("--descending-sorting", "sorting", flag_value="descending")
@click.option("--random-sorting", "sorting", flag_value="random")
@click.option("--simple-titles", "title_format", flag_value="simple", default=True)
@click.option("--simple-titles-with-rel-paths", "title_format", flag_value="simple_with_rel_path", default=True)
@click.option("--format-m3u", "output_format", flag_value="M3U")
@click.option("--format-m3u8", "output_format", flag_value="M3U8", default=True)
@click.option("--format-pls", "output_format", flag_value="PLS")
@click.option("--format-xspf", "output_format", flag_value="XSPF")
@click.option("--base-path", "base_path", type=click.Path(resolve_path=True, exists=True))
@click.argument("outfile", type=click.Path())
def main(paths, extensions, sorting, title_format, output_format, base_path, outfile):
    # TODO: Add Graphical Interface
    # TODO: Allow config files

    playlister = Playlister(paths=paths, allowed_extensions=extensions, title_type=title_format, sorting=sorting, base_path=base_path)

    if output_format == "M3U":
        playlister.write_media_info_to_m3u_file(
            outfile,
            relative_paths=True
        )
    elif output_format == "M3U8":
        playlister.write_media_info_to_m3u_file(
            outfile,
            relative_paths=True,
            encoding="utf-8"
        )
    elif output_format == "PLS":
        playlister.write_media_info_to_pls_file(
            outfile,
            relative_paths=True,
            encoding="utf-8"
        )
    elif output_format == "XSPF":
        playlister.write_media_info_to_xspf_file(
            outfile,
            relative_paths=True,
            encoding="utf-8"
        )
    else:
        raise RuntimeError("Unsupported format: M3U[8], XSPF or PLS only!")


if __name__ == '__main__':
    main()

