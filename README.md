# Playliser - Playlist creator

A very basic command line Playlist creator and library.


## Supported Features

* Export to M3U[8], PLS and XPSF
* Filter which files shall be added
* Supports either one ore multiple directorys or files or mixed paths
* Supports relative paths
* Supports complex naming rules for titles/descriptions

### Usage
```console
$ ./playlister.py -h
usage: playlister.py [-h] -p PATH [-e EXTENSION] [-s SORTING] [-t TITLE] [-f FORMAT] outfile

positional arguments:
  outfile

optional arguments:
  -h, --help            show this help message and exit
  -p PATH, --path PATH
  -e EXTENSION, --extension EXTENSION
  -s SORTING, --sorting SORTING
  -t TITLE, --title TITLE
  -f FORMAT, --format FORMAT
```